---
layout: page
title: MNT Reform system images
permalink: /images/
---

These disk images contain a bootable full Debian installation which can be
flashed to an SD-card or USB flash drive from which your Reform can then boot
directly into a full desktop installation. The image contains two partitions:
one for `/boot` and one for and `/`. U-Boot has been written to the correct
offset at the beginning of the image. Log in as `root` (empty password), change
the root password using `passwd` and create yourself a normal user using
adduser. The rescue system contains `sway` and `wayfire` for a graphical
desktops.

If you do not want to operate your Reform from the SD-card or USB stick (slow
and wears down the flash storage quickly) you can copy its contents to eMMC or
NVMe after having booted your Reform from the SD-card or USB drive. The scripts
`reform-flash-rescue`, `reform-migrate` and `reform-setup-encrypted-nvme` from
the `reform-tools` package help with that task. Alternatively, you can use <a
href="/d-i">the Debian Installer</a> to install Debian onto your Reform like
you would install Debian on any other platform.

<h3>Table of Contents</h3>

* This will become a table of contents (this text will be scrapped).
{:toc}

## MNT Reform 2 or Rack Reform

| CPU Module                              | Debian 12 Bookworm Stable | Debian 12 Bookworm Stable (backports kernel) |
|-----------------------------------------|---------------------------|----------------------------------------------|
| NXP i.MX8MQ (default)                   | <a href='reform2-system-imx8mq.img.xz'>reform2-system-imx8mq.img.xz</a> ({{ site.data.images.bookworm.reform2.imx8mq }}, <a href='reform2-system-imx8mq.img.sig'>GPG sig</a>) | <a href='reform2-system-imx8mq-bpo.img.xz'>reform2-system-imx8mq-bpo.img.xz</a> ({{ site.data.images.bookworm-backports.reform2.imx8mq }}, <a href='reform2-system-imx8mq-bpo.img.sig'>GPG sig</a>) |
| NXP Layerscape LS1028A                  | <a href='reform2-system-ls1028a.img.xz'>reform2-system-ls1028a.img.xz</a> ({{ site.data.images.bookworm.reform2.ls1028a }}, <a href='reform2-system-ls1028a.img.sig'>GPG sig</a>) | <a href='reform2-system-ls1028a-bpo.img.xz'>reform2-system-ls1028a-bpo.img.xz</a> ({{ site.data.images.bookworm-backports.reform2.ls1028a }}, <a href='reform2-system-ls1028a-bpo.img.sig'>GPG sig</a>) |
| RCM4 with Banana Pi CM4 (Amlogic A311D) | n.a.                      | <a href='reform2-system-a311d-bpo.img.xz'>reform2-system-a311d-bpo.img.xz</a> ({{ site.data.images.bookworm-backports.reform2.a311d }}, <a href='reform2-system-a311d-bpo.img.sig'>GPG sig</a>) |


## MNT Pocket Reform

| CPU Module                              | Debian 12 Stable (Bookworm) |
|-----------------------------------------|-----------------------------|
| NXP i.MX8MPlus (default)                | n.a.                    |
| RCM4 with Banana Pi CM4 (Amlogic A311D) | n.a.                    |

## How to verify GPG signature

    xzcat /path/to/reform2-system.img.xz | gpgv --keyring /usr/share/keyrings/debian-keyring.gpg --verify reform2-system.img.asc -

## How to flash to SD-card or USB flash drive

The disk image contains the bootloader (U-Boot) as well as a partition table,
so it has to be written to the SD-card or USB stick directly instead of being
copied to a mounted filesystem or written onto an existing partition. As a
result, writing the image to your SD-card or USB stick will **destroy all the
data** that was on it before. Be very certain that the device you are writing
the image to is indeed the SD-card or flash drive you inserted and not any
other disk. The following command will print the device node of any disk you
attach to your machine while the command is running:

    udevadm monitor --udev --property --subsystem-match=block/disk | sed -ne 's/^DEVNAME=//p'

You can copy the system image using `dd`. Make sure to replace
`/path/to/reform2-system.img` by the path to the system image you downloaded
and `/dev/mmcblkXXX` by the block device belonging to your SD-card or USB flash
drive.

    xzcat /path/to/reform2-system.img.xz | sudo dd of=/dev/mmcblkXXX status=progress

## non-free blobs

These images contain non-free material in the form of DDR training blobs, ARM
trusted firmware blobs, WiFi/BT firmware and/or HDMI/eDP firmware. The non-free
blobs are loaded onto the hardware early-on during the boot process and do not
run on the main processor. Sometimes the blobs are optional. For example the
HDMI blobs are only necessary if you need HDMI output. If you have your U-Boot
on eMMC, then you do not need U-Boot on your SD-card and would thus be able to
have a DFSG-free SD-card image. If you care for that, simply zero-out the
respective range of bytes between the partition table and the first partition.
If you are booting from a USB flash drive instead of from an SD-card, the
U-Boot binary will not be used.

For more information about the differences between the modules see <a
href="https://mntre.com/modularity.html#table">https://mntre.com/modularity.html#table</a>.

| CPU Module | non-free blobs |
|------------|----------------|
| NXP i.MX8MQ (default)                   | Synopsys DDR4 training blob, Cadence HDMI blob |
| NXP i.MX8MPlus                          | Synopsys DDR4 training blob, WiFi/BT firmware |
| NXP Layerscape LS1028A                  | eDP display firmware blob |
| RCM4 with Banana Pi CM4 (Amlogic A311D) | ARM trusted firmware, WiFi firmware |

## Source

The images are generated by the following scripts: <a
href="https://salsa.debian.org/reform-team/reform-system-image/">https://salsa.debian.org/reform-team/reform-system-image</a>
